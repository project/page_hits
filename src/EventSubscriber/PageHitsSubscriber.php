<?php

namespace Drupal\page_hits\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Logs page hits.
 */
class PageHitsSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The admin context service.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The session manager service.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructor for subscriber.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config_factory service object.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   *   The time service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(AccountInterface $current_user, Connection $database, ConfigFactoryInterface $config_factory, AdminContext $admin_context, TimeInterface $time, SessionManagerInterface $session_manager, RouteMatchInterface $route_match) {
    $this->currentUser = $current_user;
    $this->database = $database;
    $this->configFactory = $config_factory;
    $this->adminContext = $admin_context;
    $this->time = $time;
    $this->sessionManager = $session_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::TERMINATE][] = ['log', 100];
    return $events;
  }

  /**
   * Log page hits.
   *
   * @param \Symfony\Component\HttpKernel\Event\KernelEvent $event
   *   The event.
   */
  public function log(KernelEvent $event) {
    $enabledForAdminRole = $this->configFactory->get('page_hits.settings')->get('increment_page_count_for_admin');
    $admin = in_array('administrator', $this->currentUser->getRoles());
    if ($admin && !$enabledForAdminRole) {
      return;
    }

    $isAdminInterface = $this->adminContext->isAdminRoute();
    if ($isAdminInterface || !$event->getResponse() instanceof HtmlResponse) {
      return;
    }

    $request = $event->getRequest();
    $node = $this->routeMatch->getParameter('node');

    global $base_url;
    $fields = [
      'ip' => $request->getClientIp(),
      'session_id' => $this->sessionManager->getId(),
      'url' => $base_url . $request->getRequestUri(),
      'uid' => $this->currentUser->id(),
      'nid' => $node instanceof NodeInterface ? $node->id() : 0,
      'created' => $this->time->getRequestTime(),
    ];

    // Insert the record to table.
    $this->database->insert('page_hits')->fields($fields)->execute();
  }

}
