<?php

namespace Drupal\page_hits\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'page_hits' block.
 *
 * @Block(
 *   id = "page_hits_block",
 *   admin_label = @Translation("Page Hits"),
 *   category = @Translation("Page Hits block")
 * )
 */
class PageHitsBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The Page hits's default configuration settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new self($configuration, $plugin_id, $plugin_definition);
    $instance->config = $container->get('config.factory')->get('page_hits.settings');
    $instance->account = $container->get('current_user');
    $instance->requestStack = $container->get('request_stack');
    $instance->database = $container->get('database');
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    global $base_url;
    $currentPageUrl = $base_url . $this->requestStack->getCurrentRequest()->getRequestUri();

    $node = $this->routeMatch->getParameter('node');
    $result = $node instanceof NodeInterface ? $this->getPageHitsByNode($node) : $this->getPageHitsByUrl($currentPageUrl);

    $output = '<ul>';
    if ($this->config->get('show_user_ip_address')) {
      $output .= '<li>' . $this->t('YOUR IP: <strong>%ip</strong>', ['%ip' => $this->requestStack->getCurrentRequest()->getClientIp()]) . '</li>';
    }
    if ($this->config->get('show_unique_page_visits')) {
      $output .= '<li>' . $this->t('UNIQUE VISITORS: <strong>%uniqueVisits</strong>', ['%uniqueVisits' => $result['uniqueVisits'] ?? 0]) . '</li>';
    }
    if ($this->config->get('show_total_page_count')) {
      $output .= '<li>' . $this->t('TOTAL VISITS: <strong>%totalVisitors</strong>', ['%totalVisitors' => $result['totalVisitors'] ?? 0]) . '</li>';
    }
    if ($this->config->get('show_page_count_of_logged_in_user') &&  $this->account?->id() ?? NULL) {
      $output .= '<li>' . $this->t('TOTAL VISITS BY YOU: <strong>%totalVisitsByYou</strong>', ['%totalVisitsByYou' => $result['totalVisitsByYou'] ?? 0]) . '</li>';
    }
    if ($this->config->get('show_total_page_count_of_week')) {
      $output .= '<li>' . $this->t('TOTAL VISITS IN THIS WEEK: <strong>%totalVisitorsInThisWeek</strong>', ['%totalVisitorsInThisWeek' => $result['totalVisitorsInThisWeek'] ?? 0]) . '</li>';
    }
    $output .= '</ul>';
    $build['#markup'] = '<div id="page-hits-counter">' . $output . '</div>';
    $build['#cache']['max-age'] = 0;
    $build['#allowed_tags'] = [
      'div', 'script', 'style', 'link', 'form',
      'h2', 'h1', 'h3', 'h4', 'h5',
      'table', 'thead', 'tr', 'td', 'tbody', 'tfoot',
      'img', 'a', 'span', 'option', 'select', 'input',
      'ul', 'li', 'br', 'p', 'link', 'hr', 'style', 'img',
    ];

    return $build;
  }

  /**
   * Get page hits by node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node which is being viewed.
   */
  private function getPageHitsByNode(NodeInterface $node) {
    $query = $this->database->select('page_hits', 'p');
    $query->fields('p');
    $query->condition('p.nid', $node->id());
    $result = $query->execute()->fetchAll();
    if (empty($result)) {
      return [];
    }

    return $this->calculateStats($result);
  }

  /**
   * Get page hits by URL.
   *
   * @param string $pageUrl
   *   The URL of the page being viewed.
   */
  private function getPageHitsByUrl(string $pageUrl) {
    $query = $this->database->select('page_hits', 'p');
    $query->fields('p');
    $query->condition('p.url', $pageUrl);
    $result = $query->execute()->fetchAll();
    if (empty($result)) {
      return [];
    }

    return $this->calculateStats($result);
  }

  /**
   * Calculate Stats.
   */
  private function calculateStats(array $data) {
    $stats = [
      'uniqueVisits' => 0,
      'totalVisitsByYou' => 0,
      'totalVisitorsInThisWeek' => 0,
      'totalVisitors' => count($data),
    ];

    $uniqueVisits = [];
    foreach ($data as $record) {
      $uniqueVisits[$record->session_id] = $record->session_id;

      if ($uid = $this->account?->id() ?? NULL) {
        if ($record->uid == $uid) {
          $stats['totalVisitsByYou'] += 1;
        }
      }

      $firstDayOfTheWeek = 'Sunday';
      $startOfTheWeek = strtotime("Last $firstDayOfTheWeek");
      if (strtolower(date('l')) === strtolower($firstDayOfTheWeek)) {
        $startOfTheWeek = strtotime('today');
      }
      $endOfTheWeek = $startOfTheWeek + (60 * 60 * 24 * 7) - 1;
      if ($record->created >= $startOfTheWeek && $record->created <= $endOfTheWeek) {
        $stats['totalVisitorsInThisWeek'] += 1;
      }
    }

    $stats['uniqueVisits'] = count($uniqueVisits);

    return $stats;
  }

}
